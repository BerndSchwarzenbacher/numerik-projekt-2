linkfollow_prob=0.85;

clf;

dimensions = [10, 20, 50, 100, 200, 500, 1000];
nr_entries = size(dimensions, 2);
eigenvalues = zeros(1, nr_entries);

for i = 1:nr_entries
  rand_mat = randi([0 1], dimensions(i));
  g_matrix = google_matrix(rand_mat, linkfollow_prob);
  eig_val = sort( abs(eig(g_matrix)));
  eigenvalues(i) = eig_val(end-1);
end

plot (dimensions, eigenvalues, 'LineWidth', 5);

xl = xlabel('Dimension n');
set(xl, 'fontsize', 20);
set(gca, 'fontsize', 20);

yl = ylabel('Betrag vom zweiten Eigenwert');
set(yl, 'fontsize', 20);

saveas(1, 'eigenvalues.png');

