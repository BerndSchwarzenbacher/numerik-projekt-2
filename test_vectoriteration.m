if exist('dim') && dim > 0

rand_mat = randi([0 1], dim);

[g_vector, errors] = vectoriteration(rand_mat, linkfollow_prob)
google_mat = google_matrix(rand_mat, linkfollow_prob)

else

  'Please specify a dimension "dim"'
  'and a linkfollow probability "linkfollow_prob"'

end

