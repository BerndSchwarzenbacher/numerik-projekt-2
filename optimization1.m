L = [[0,0,0,1,0,1,0];
     [1,0,0,0,0,0,0];
     [1,0,0,0,0,0,0];
     [0,1,0,0,1,0,1];
     [0,1,0,1,0,0,1];
     [0,0,0,1,0,0,0];
     [0,1,0,0,0,1,0]];

dim=size(L,1);                 
L(8,:)=[0,0,1,0,0,0,0];        
L(:,8)=zeros(8,1);
E=eye(8);                      
L_ver1=L;                      
help=zeros(7,1);
x=1;
a=0;
b=0;

while x==1                  
    for i=1:dim                                
        if L(i,8)==0                          
            L(:,8)=+E(:,i);                 
            erg=webrank(L);                   
            help(i)=erg(find(erg(:,1)==3),2);  
            L=L_ver1;                          
        end                                    
    end

    j=find(help == max(help));           
    L(:,8)=+E(:,j);                    
    erg=webrank(L);                    
    a=erg(find(erg(:,1)==3),2);        
    
    if a>b                  
        L_ver1=L;        
    else                     
        x=0;              
    end                     
    
    help(j)=0;                          
    erg_new=webrank(L_ver1);          
    b=erg_new(find(erg_new(:,1)==3),2); 
end

