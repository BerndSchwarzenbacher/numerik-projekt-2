function google_mat = google_matrix(adjacent_mat, linkfollow_prob)

  nr_sites   = size(adjacent_mat, 1);
  google_mat = (1-linkfollow_prob) / nr_sites * ones(nr_sites, nr_sites);


  for j = 1:nr_sites

    outdegree = sum( adjacent_mat(:,j) );

    for i = 1:nr_sites
      if adjacent_mat(i,j)
        google_mat(i, j) =+ linkfollow_prob * 1/outdegree;
      end
    end

  end


end

