function [weight_vector, iterator_step] = vectoriteration_new (adjacent_mat, linkfollow_prob)

  dim = size(adjacent_mat, 1);
  help_vec_1=((1-linkfollow_prob)/dim)*ones(dim,1);
  pT=zeros(dim,dim);
  
  for j = 1:dim
     outdegree = sum( adjacent_mat(:,j) );
     for i = 1:dim
       if adjacent_mat(i,j)==1
         pT(i, j) = linkfollow_prob * 1/outdegree;
       end
     end
  end
  
  
  old_vector = rand(dim,1);
  help_vec_2=zeros(dim,1);
    for i=1:dim
        for j=1:dim
            if pT(i,j) ~= 0
                help_vec_2(i) =+ pT(i,j)*old_vector(j);
            end
        end
    end
  tmp_vector=help_vec_1+help_vec_2;
  weight_vector = 1 / norm(tmp_vector, 1) * tmp_vector;
  iterator_step = norm(weight_vector-old_vector, 1);

  while iterator_step(end) > 1e-12
    old_vector = weight_vector;
    help_vec_2=zeros(dim,1);
    for i=1:dim
        for j=1:dim
            if pT(i,j) ~= 0
                help_vec_2(i) =+ pT(i,j)*old_vector(j);
            end
        end
    end
    tmp_vector=help_vec_1+help_vec_2;
    weight_vector = 1 / norm(tmp_vector, 1) * tmp_vector;

    iterator_step(end+1) = norm(weight_vector-old_vector, 1);
  end

end

