% Fuer Aufgabe e

linkfollow_prob=0.85;

clf
hold all;
for i = 1:3
  dim = 10^i;
  rand_mat = randi([0 1], dim);

  [g_vector, errors] = vectoriteration(rand_mat, linkfollow_prob);
  semilogy(errors, 'LineWidth', 5)

end

grid;

leg = legend('10', '100', '1000');
set(leg, 'fontsize', 20);

xl = xlabel('Iteration k');
set(xl, 'fontsize', 20);
set(gca, 'fontsize', 20);

yl = ylabel('Residuen');
set(yl, 'fontsize', 20);

saveas(1, 'figure1.png');

