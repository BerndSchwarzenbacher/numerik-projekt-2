function [weight_vector, iterator_step] = \
  vectoriteration (adjacent_mat, linkfollow_prob)

  dim = size(adjacent_mat, 1);
  google_mat = google_matrix(adjacent_mat, linkfollow_prob);

  old_vector = rand(dim, 1);
  tmp_vector = google_mat * old_vector;
  weight_vector = 1 / norm(tmp_vector, 1) * tmp_vector;

  iterator_step = norm(weight_vector-old_vector, 1);

  while iterator_step(end) > 1e-12
    old_vector = weight_vector;
    tmp_vector = google_mat * old_vector;
    weight_vector = 1 / norm(tmp_vector, 1) * tmp_vector;

    iterator_step(end+1) = norm(weight_vector-old_vector, 1);
  end

end

