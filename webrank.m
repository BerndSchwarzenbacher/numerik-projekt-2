function [ranking] = webrank(L)

  [g_vector, errors] = vectoriteration(L, 0.85);
  count = [1:size(L,1)]';
  ranking = [count, g_vector];
  ranking = sortrows(ranking, -2);

end

