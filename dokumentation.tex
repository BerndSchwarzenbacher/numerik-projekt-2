\documentclass[a4paper, 12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{amsmath, amssymb}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{float}
\usepackage{commath}

\newcommand{\g}{\mathbf{g}}

\renewcommand{\labelenumi}{\alph{enumi}.)}
\renewcommand{\figurename}{Abbildung}

\lstset{numbers=left, numberstyle=\tiny, numbersep=5pt}
\lstset{language=Matlab}

\begin{document}

\title{Numerische Mathematik UE WS 14/15 Projekt 2 / Teilprojekt 3}
\author{Martin Hofbauer, Bernd Schwarzenbacher}
\date{12. Februar 2015}
\maketitle
\pagebreak

\section{Theoretischer Teil}

\subsection{Theoretische Aufgaben}

\begin{enumerate}

\item \textbf{Behauptung:} Für $p < 1$ hat die Google-Matrix
$G := ( \frac{1-p}{N} E + p T )$ nur positive Einträge und ist
spalten-stochastisch.

\textbf{Beweis:} Wegen $p < 1$ ist $1 - p > 0$. Auserdem sind alle Einträge von
$T$ positiv und somit sind alle Einträge von $G$ positiv.\\
$C_{j}$ ist die Anzahl der Verlinkungen von
Seite $j$. In $T_{ij}$ steht nur dann $\frac{1}{C_{j}}$, wenn eine Verlinkung
von Seite $j$ zur Seite $i$ existiert und insgesamt sind das genau $C_{j}$
Verlinkungen von Seite $j$.
Für eine beliebige Spalte $j$ der Google-Matrix $G$ gilt daher:
\begin{eqnarray*}
  \sum_{i=1}^{N} G_{ij}
  & = & \sum_{i=1}^{N} \left( \frac{1-p}{N} E + p T \right)_{ij} \\
  & = & \sum_{i=1}^{N} \frac{1-p}{N} + p \sum_{i=1}^N T_{ij} \\
  & = & 1 - p + p \sum_{i = 1}^{N} \frac{L_{ij}}{C_{j}} \\
  & = & 1 - p + p \, C_{j} \frac{1}{C_{j}} = 1
\end{eqnarray*}
Also ist die Matrix $G$ spalten-stochastisch.


\item \textbf{Behauptung:} Jede spalten-stochastische Matrix $G$ hat $1$ als
Eigenwert.

\textbf{Beweis:}
Sei $\mathbf{e} = (1, \ldots, 1)^{T}$
\begin{equation*}
  G^{T} \mathbf{e} =
  (\sum_{j=1}^{N} G^{T}_{1j}, \cdots, \sum_{j=1}^{N} G^{T}_{Nj} )^{T}
  = (\sum_{j=1}^{N} G_{j1}, \cdots, \sum_{j=1}^{N} G_{jN} )^{T}
  = \mathbf{e}
\end{equation*}
Wegen $detA = detA^T$ folgt unter Betrachtung des charakterstischen Polynoms,
dass $1$ als Eigenwert der transponierten Matrix auch Eigenwert der Matrix
selbst ist.


\item \textbf{Behauptung:} Jeder Eigenvektor $\mathbf{v}$ zum Eigenwert $1$
von $G$ hat nur positive oder nur negative Einträge.

\textbf{Beweis:}
Angenommen die Komponenten $\mathbf{v}_{i}$ des Eigenvektors haben verschiedene
Vorzeichen, dann folgt ausgehend von der Eigenwertgleichung mithilfe der
Dreiecksungleichung und der Tatsache, dass $G$ nur positive Einträge besitzt:
\begin{eqnarray*}
  \sum_{j} G_{ij} \mathbf{v}_{j} & = & \mathbf{v}_{i} \\
  |\sum_{j} G_{ij} \mathbf{v}_{j}| & = & |\mathbf{v}_{i}| \\
  \sum_{j} G_{ij} |\mathbf{v}_{j}| & \geq & |\mathbf{v}_{i}|
\end{eqnarray*}
Die Gleichheit tritt hier nur dann ein, wenn alle Vorzeichen positiv bzw.\
negativ sind. Laut Annahme gilt daher die strikte Ungleichung:
\begin{equation*}
  \sum_{j} G_{ij} |\mathbf{v}_{j}| > |\mathbf{v}_{i}|
\end{equation*}
Nun erhält man den Widerspruch durch Summieren über $i$, weil $G$
spaltenstochastisch ist:
\begin{eqnarray*}
  \sum_{i} \sum_{j} G_{ij} |\mathbf{v}_{j}| & > & \sum_{i} |\mathbf{v}_{i}| \\
  \sum_{j} |\mathbf{v}_{j}| \sum_{i} G_{ij} & > & \sum_{i} |\mathbf{v}_{i}| \\
  \sum_{j} |\mathbf{v}_{j}| & > & \sum_{i} |\mathbf{v}_{i}|
\end{eqnarray*}

\item \textbf{Behauptung:} Zu zwei linear unabhängigen Vektoren $\mathbf{v}$
und $\mathbf{w}$ existieren Zahlen $s$ und $t$, sodass
$x := s \mathbf{v} + t \mathbf{w}$ positive und negative Einträge besitzt.

\textbf{Beweis:}
Es genügt, die Behauptung für Vektoren
$\mathbf{v}, \mathbf{w} \in \mathbb{R}^{2}$ zu zeigen, da Vektoren im
$\mathbb{R}^{2}$ zu beliebiger Größe erweitert werden können.
Seien also $\mathbf{v}, \mathbf{w} \in \mathbb{R}^{2}$ linear unabhängig und
beliebig und
$\mathbf{v} = \left( \begin{array}{c} v_1 \\ v_2 \end{array} \right)$
bzw.\
$\mathbf{w} = \left( \begin{array}{c} w_1 \\ w_2 \end{array} \right)$. \\
Wähle $s = \frac{1}{ \sqrt{v_1^{2} + v_2^{2}} }$ und
$t = -\frac{1}{ \sqrt{w_1^{2}+w_2^{2}} }$, somit ergeben sich unter der Annahme
$v_1 w_2 < v_2 w_1$:

\begin{eqnarray*}
  v_1w_2 & < & w_1v_2 \\
  v_1^2w_1^2+v_1^2w_2^2 & < & w_1^2v_1^2+w_1^2v_2^2 \\
  v_1\sqrt{w_1^{2}+w_2^{2}} & < & w_1\sqrt{v_1^{2}+v_2^{2}} \\
  \frac{v_1}{\sqrt{v_1^{2}+v_2^{2}}} & < & \frac{w_1}{\sqrt{w_1^{2}+w_2^{2}}}
\end{eqnarray*}

beziehungsweise:
\begin{eqnarray*}
  v_2w_1 & > & w_2v_1 \\
  v_2^2w_1^2+v_2^2w_2^2 & >& w_2^2v_1^2+w_2^2v_2^2 \\
  v_2\sqrt{w_1^{2}+w_2^{2}} & > & w_2\sqrt{v_1^{2}+v_2^{2}} \\
  \frac{v_2}{\sqrt{v_1^{2}+v_2^{2}}} & > & \frac{w_2}{\sqrt{w_1^{2}+w_2^{2}}}
\end{eqnarray*}

Damit ergibt sich nun:
\begin{eqnarray*}
\frac{v_1}{\sqrt{v_1^{2}+v_2^{2}}} - \frac{w_1}{\sqrt{w_1^{2}+w_2^{2}}} & < & 0 \\
\frac{v_2}{\sqrt{v_1^{2}+v_2^{2}}} - \frac{w_2}{\sqrt{w_1^{2}+w_2^{2}}} & > & 0
\end{eqnarray*}
Somit besitzt der Ergebnisvektor einen positive und einen negativen Eintrag. \\
Den Fall $v_1w_2 > v_2w_1$ behandelt man analog.
Der Fall $v_1w_2 = v_2w_1$ wird voraussetzungsgemäß durch die Annahme der
linearen Un\-ab\-hän\-gig\-keit ausgeschlossen.


\stepcounter{enumi}
\item \textbf{Frage:} Wieviele Operationen benötigt ein Iterationsschritt der
Vektoriteration bei einer Dimension $N$, wenn man annimmt, dass jede Seite
maximal $K \in \mathbb{N}$ Links auf andere Seiten enthält.

Betrachten wir die Multiplikation $G \g = ( \frac{1-p}{N} E + p T ) \g$
als getrennte Multiplikationen $(\frac{1-p}{N} E)\g$ und $(pT)\g$.
Die erste Multiplikation besteht aus einer $N \! \times \! N$-Matrix
mit ausschließlich gleichen Einträgen und dem Gewichtsvektor $\g$.
Für diese Multiplikation werden keine Rechenoperationen benötigt weil $\g$
ein bezüglich der 1-Norm normierter Vektor ist und somit der Ergebnisvektor
ausschließlich aus Einträgen mit Wert $\frac{1-p}{N}$ besteht.
Damit besteht der Ergebnisvektor aus $N$ gleichen Einträgen.
Die zweite Multiplikation besteht aus aus einer $N \! \times \! N$-Matrix mit
insgesamt $N K$ Einträgen ungleich $0$ und dem Gewichtsvektor $\g$.
Für diese Multiplikation ergibt sich ein Aufwand von $N K$  Operationen.
Zur Addition der beiden Vektoren werden dann noch $N$ Additionen benötigt.
Dieser Vektor ist jedoch noch nicht normiert.
Die Normierung benötigt wiederum $N$ Additionen für die Berechnung der Norm
und $N$ Multiplikationen (Divisionen) für die Divisionen des Vektors durch
seine Norm, also $N+N = 2N$ Operationen.
Insgesamt erhalten wir somit $NK + N + 2N = N (4+K)$ Operationen. Bei
einem fixen Wert für $K$ ergibt sich also ein Aufwand von $\mathcal{O}(N)$.


\end{enumerate}


\subsection{Theoretische Schlussfolgerungen}
Wir haben in a-d gezeigt, dass die Matrix G nur Einträge zwischen $0$ und $1$
besitzt die sich spaltenweise zu $1$ addieren. Daraus lässt sich folgern,
dass die Matrix $G$ den Eigenwert $1$ besitzt und jeder zugehörige Eigenvektor
nur positive oder nur negative Einträge besitzt. Betrachtet man nun den
Eigenraum zum Eigenwert $1$, lässt sich über die Annahme def$(G-E) \ge 2$ leicht
ein Widerspruchsbeweis bilden: zwei linear unabhängige Basisvektoren könnten
laut d) durch eine geeignete Linearkombination einen Vektor mit positiven und
negativen Einträgen erzeugen, was aber in Widerspruch zu c) stünde.
Daher ist die Dimension des Eigenraums zum Eigenwert $1$ gleich $1$. \\
Damit liefert uns die Vektoriteration einen eindeutigen Eigenvektor zum
Eigenwert $1$.


\section{Experimenteller Teil}

\subsection{Planung der Experimente}

\subsubsection{Verhalten der Iteration bei steigender Größe der Adjazenzmatrix:}
Zur Berechnung des Eigenvektors wird der Vektoriteration-Algorithmus
implementiert. Abgebrochen werden soll bei einem Residuum von\\
$\| \g^{(k)} - \g^{(k-1)} \|_{1} \leq 10^{-12}$.
Die Residuen der einzelnen Iterationsschritte sollen für zufällige
Adjazenzmatrizen verschiedener Dimension verglichen werden. Daraus sollen dann
Schlussfolgerungen über den zweitgrößten Eigenwert von $G$ getroffen werden.

\subsubsection{Netzwerkoptimierung - Manipulation der Website 8}
Wie angegeben verlinken wir von Seite 3 auf Seite 8. Mithilfe des
Optimierungs-Algorithmus (Listing~\ref{lst.opt1}) versuchen wir nun die
bestmögliche $8 \! \times \! 8$-Adjazenzmatrix zu finden, wobei wir nur die
8.~Spalte manipulieren können. Der Algorithmus "probiert" die verschiedenen
Verlinkungen der Reihe nach durch und überprüft mit welcher Verlinkung die beste
Reihung für Seite 3 erzielt wird.

\subsubsection{Netzwerkoptimierung - Manipulation der Websites 3 und 8}
Zusätzlich zu den Verlinkungen von Seite 8 nehmen wir an, dass wir ebenfalls
(realistischerweise) die Links von Website 3 beeinflussen können.
Im zweiten Optimierungs-Algorithmus (Listing~\ref{lst.opt2}) wird daher nach
der Optimierung der Verlinkungen von Website 8 noch die gleiche Optimierung auf
Seite 3 angwendet.

\subsection{Resultate und Schlussfolgerungen}

\subsubsection{Verhalten der Iteration bei steigender Größe der Adjazenzmatrix:}
\begin{figure}[H]
  \includegraphics[width=1\textwidth]{figure1.png}
  \caption{Konvergenzverhalten von Google-Matrizen mit Dimension $10/100/1000$,
    dargestellt durch die Residuen  $\| \g^{(k)} - \g^{(k-1)} \|_{1}$ über die
    Iterationsanzahl $k$.}
\label{fig.residuen}
\end{figure}

Wie in Abbildung~\ref{fig.residuen} zu erkennen ist, sinkt die Iterationsanzahl
des Algorithmus bei steigender Dimension der Google-Matrix.
Im Skriptum findet sich zum Konvergenzverhalten der Vektoriteration
folgende Analysis (Gleichung~9.6):
\begin{equation*}
  \norm{ \g^{(k)}
    - \frac{ \lambda_{1}^{k} \g_{1}^{(0)} }{ | \lambda_{1}^{k} \g_{1}^{(0)} | }
    \mathbf{v}_{1} }
  = \mathcal{O} \left( \left|
  \frac{\lambda_{2}}{\lambda_{1}} \right|^{k} \right) \qquad k \to \infty
\end{equation*}
Wobei $\mathbf{v}_{1}$ der normierte Eigenvektor der Google-Matrix zum Eigenwert
$1$ ist.

Also hängt die Konvergenzgeschwindigkeit der Vektoriteration vom Verhältnis von
$\lambda_{2}$ zu $\lambda_{1}$ ab. Da der erste Eigenwert aber laut Theorieteil
bei $1$ liegt, müsste der zweite Eigenwert bei steigender Dimension kleiner
werden. In Abbildung~\ref{fig.secEv} erkennt man genau dieses Verhalten.

\begin{figure}[H]
  \includegraphics[width=1\textwidth]{eigenvalues.png}
  \caption{Zweiter Eigenwert von Google-Matrizen mit Dimension $n$}
\label{fig.secEv}
\end{figure}


\subsubsection{Netzwerkoptimierung - Manipulation der Website~8}
Als Ergebnis erhält man folgende Reihung: Von Seite~8 sollte lediglich zur
Seite~3 ein Link zurückführen. Dieses auf den ersten Blick unerwartete Verhalten
der Gewichte ist dadurch zu erklären, dass so das Gewicht der Seiten~3 und 8
lediglich auf die jeweils andere Seite aufgeteilt wird.

\subsubsection{Netzwerkoptimierung - Manipulation der Websites~3 und 8}
Wie im vorigen Punkt empfielt eine Verlinkung von Seite~8 zu 3. Für Seite~3
jedoch wäre keine zusätzliche Verlinkung zu empfehlen, da sich sonst das Gewicht
der Seite~3 auf die Seite~8 und die anderen verlinkten Seiten aufteilt
und somit auch weniger Gewicht über die Verlinkung von 8 zu 3 "zurückfließt".

\subsection{Beschreibung der Betriebsmittel}

\subsubsection{Fertigsoftware}
GNU Octave 3.8.2

\subsubsection{Listing selbst entwickelter Algorithmen}
\lstinputlisting[caption={vectoriteration.m / nach Algorithmus 9.1 im Skriptum},
  label={lst.vectorit}]{vectoriteration.m}
\lstinputlisting[caption={google-matrix.m zur Erstellung der Matrix G},
label={lst.vectorit}]{google_matrix.m}
\lstinputlisting[caption={optimization1.m / Optimierung durch Verlinkungen von
  Seite 8}, label={lst.opt1}]{optimization1.m}
\lstinputlisting[caption={optimization2.m / Optimierung mit zusätzlichen
  Verlinkungen von Seite 3}, label={lst.opt2}]{optimization2.m}

\subsubsection{Computersystem}
Sämtliche Berechnungen wurden auf folgendem System durchgeführt:\\
Lenovo, Thinkpad T440s Intel Core i7-4600U 12GB Ram\\
Betriebssystem: x86\_64 GNU/Linux 3.18.6-1-ARCH

\end{document}

